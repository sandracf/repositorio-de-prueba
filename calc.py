#  Creamos la funcion suma
def sumar(a, b):
    return a + b
#  Creamos la funcion resta
def restar(a, b):
    return a - b

#  Sumamos 1 + 2
print("El resultado de la suma es: " + str(sumar(1, 2)))

#  Sumamos 3 + 4
print("El resultado de la suma es: " + str(sumar(3, 4)))

#  Restamos 6 - 5
print("El resultado de la resta es: " + str(restar(6, 5)))

#  Restamos 8- 7
print("El resultado de la resta es: " + str(restar(8, 7)))